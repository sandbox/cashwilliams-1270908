<?php
/**
 * A handler to provide proper displays for dates.
 *
 * @ingroup views_field_handlers
 */
class caldav_handler_field_date extends views_handler_field_date {

  /**
   * Called to add the field to a query.
   */
  function query() {
    // $this->ensure_my_table();
    // Add the field.
    // $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field);
    // $this->add_additional_fields();
    
    $this->field_alias = $this->real_field;
  }
  
  function render($values) {
    
    $value = $values->{$this->field_alias};
        
    if(is_string($value) && strpos($value,"-")) {
        $value = strtotime($value);

        if($value) {
            $values->{$this->field_alias} = $value;
        }
    }

    return parent::render($values);
  }
  
}


