<?php

class caldav_plugin_query_event extends views_plugin_query {

  /**
   * Constructor; Create the basic query object and fill with default values.
   */
  function init($base_table = '', $base_field, $options = array()) {
    parent::init($base_table, $base_field, $options);
  }

  function use_pager() {
    return FALSE;
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($view, $get_count = FALSE) {
    return array();
  }

  function add_param($param, $value = '') {
    $this->params[$param] = $value;
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    //$view->init_pager($view);

    // Let the pager modify the query to add limits.
    //$this->pager->query();
    
    $view->build_info['query'] = $this->query($view);
    $view->build_info['count_query'] = $this->query($view, TRUE);
    $view->build_info['query_args'] = $this->get_where_args();
  }

  function execute(&$view) {
    $start = views_microtime();
    
    global $user;
  
    require_once('./'. drupal_get_path('module', 'caldav') .'/includes/ical_parser.inc');
    require_once('./'. drupal_get_path('module', 'date_api') .'/date_api_ical.inc');
    
    $calendar = new ical_parser();

    $calendar->load_events($user->caldav_url);
    
    $results = $calendar->get_events();
    
    if($results) {
      $view->result = $results;
      // $this->results are arrays.
      foreach ($view->result as $key => $value) {  
        $view->result[$key] = (object) $value;
      }

      // Save the metadata into the object
      unset($results['results']);
      foreach ($results as $key => $value) {
        $this->$key = $value;
      }
    }
    $view->total_rows = $calendar->get_events_count();
    
    /*
    if (!empty($this->orderby)) {
      foreach (array_reverse($this->orderby) as $orderby) {
        // order result set
      }
    }
    */
    
    $view->execute_time = views_microtime() - $start;
  }

  function add_signature(&$view) {}

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    // make needed changes to $form
  }
  
  function options_validate(&$form, &$form_state) {
  }

  function options_submit(&$form, &$form_state) {
  }

  function add_field($table, $field, $alias = '', $params = array()) {
    
    $alias = $field;

    // Create a field info array.
    $field_info = array(
      'field' => $field,
      'table' => $table,
      'alias' => $field,
    ) + $params;

    if (empty($this->fields[$field])) {
      $this->fields[$field] = $field_info;
    }

    return $field;
    
  }

  function add_orderby($table, $field, $order, $alias = '', $params = array()) {
    $this->orderby[] = array(
      'field' => $field,
      'order' => $order,
    );
  }

  function add_filter($group, $operator, $xpath_selector, $value) {
    /*
    $this->filter[] = array(
      'group' => $field,
      'operator' => $operator,
      'xpath_selector' => $xpath_selector,
      'value' => $value,
    );
    */
  }
  
  function add_where($group, $clause) {
    
  }
  
  /**
   * Get the arguments attached to the WHERE and HAVING clauses of this query.
   */
  function get_where_args() {
    $args = array();
    return $args;
  }
}
