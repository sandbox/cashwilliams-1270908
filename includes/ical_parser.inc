<?php

/*
 * This file is an include for parsing CalDAV feeds
 */

require_once('./'. drupal_get_path('module', 'date_api') .'/date_api_ical.inc');

class ical_parser
{
  
  var $events;
  var $event_count;
  
  function load_events($url) {
    
    //$temp = $this->ical_import($url);
    $temp = array();
    
    $temp = $temp[0]['VEVENT'];

    $timezone = date_default_timezone_name(); //returns string

    for($i = 0; $i < count($temp); $i++) {

      $this->events[$i]['title'] = $temp[$i]['SUMMARY'];
      $this->events[$i]['description'] = $temp[$i]['DESCRIPTION'];
      $this->events[$i]['location'] = $temp[$i]['LOCATION'];
      //$this->events[$i]['start'] = date_ical_date($temp[$i]['DTSTART'], $timezone)->getTimestamp();
      //$this->events[$i]['end'] = date_ical_date($temp[$i]['DTEND'], $timezone)->getTimestamp();
      $this->events[$i]['start_date'] = $temp[$i]['DTSTART']['datetime'];
      $this->events[$i]['end_date'] = $temp[$i]['DTEND']['datetime'];
      $this->events[$i]['tz'] = $temp[$i]['DTSTART']['tz'];
      $this->events[$i]['status'] = $temp[$i]['STATUS'];
      $this->events[$i]['all_day'] = $temp[$i]['all_day'];
    }
    $this->event_count = $i+1;
  }
    
  function get_events() {
    return $this->events;
  }
  
  function get_events_count() {
    return $this->event_count;
  }
  
  
 function ical_import($url) {
  
    $uri = parse_url($url);
    $url = $uri['scheme'].'://'.$uri['host'].$uri['path'].$uri['query'];
    //$realm = 'd.brauerranch.com';
    $realm = $uri['host'];

    //open connection
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $digest_info = array();
    $result = curl_exec($ch);
    $pat = '/WWW-Authenticate:(.*)/';
    preg_match_all($pat, $result, $matches);
    if (isset($matches[1])) {
      foreach ($matches[1] as $match) {
        if (strpos($match, ' digest nonce') === 0) {
          $pat = '/(\w+)="([^"]+)"/';
          preg_match_all($pat, $result, $ms);
          foreach ($ms[1] as $k => $m) {
            $digest_info[$m] = $ms[2][$k];
          }
        }
      }
    }

    $headers = array(
      'WWW-Authenticate' => 'Digest realm="' . $digest_info['realm'] .'",qop="auth", nonce="' . $digest_info['nonce'] . '",opaque="' . md5($digest_info['realm']) . '"'
    );
    //set the url, auth type, and user/pass
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_USERPWD, $uri['user'].':'.$uri['pass']);

    //execute post
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);
    //close connection
    curl_close($ch);
    
    list($header, $body) = explode("\r\n\r\n", $result, 2);
    
    //TODO: check the $headers for response here
    
    $icaldatafolded = explode("\n", $body);
    
    // Verify this is iCal data
    if (trim($icaldatafolded[0]) != 'BEGIN:VCALENDAR') {
      watchdog('date ical', 'Invalid calendar file: %filename', array('%filename' => $filename));
      return false;
    }
    return date_ical_parse($icaldatafolded);
  }
 
}

