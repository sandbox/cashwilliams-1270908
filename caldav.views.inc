<?php

/**
 * Implements hook_views_data
 */
function caldav_views_data() {
  $data = array();
  
  $data['caldav']['table']['group'] = t('CalDAV');

  $data['caldav']['table']['base'] = array(
    'title' => t('CalDAV'),
    'help' => t('Queries an external CalDAV source.'),
    'query class' => 'caldav_query',
  );

  $data['caldav']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title or summary of an event'),
    'field' => array(
      'handler' => 'caldav_handler_field',
    ),
  );
  
  $data['caldav']['start_date'] = array(
    'title' => t('Start Date'), 
    'help' => t('The start time of the event.'), 
    'field' => array(
      'handler' => 'caldav_handler_field_date',
      'base' => 'caldav',
    ), 
    //'sort' => array(
    //  'handler' => 'views_handler_sort_date',
    //), 
    //'filter' => array(
    //  'handler' => 'date_api_filter_handler',
    //),
    'argument' => array(
      'handler' => 'date_api_argument_handler',
      'base' => 'caldav',
     ),
  );
  
  $data['caldav']['end_date'] = array(
    'title' => t('End Date'), 
    'help' => t('The end time of the event.'), 
    'field' => array(
      'handler' => 'caldav_handler_field_date',
      'base' => 'caldav',
    ), 
    //'sort' => array(
    //  'handler' => 'views_handler_sort_date',
    //), 
    //'filter' => array(
    //  'handler' => 'date_api_filter_handler',
    //),
    'argument' => array(
      'handler' => 'date_api_argument_handler',
      'base' => 'caldav',
    ),
  );
  
  return $data;
}


/**
 * Implementation of hook_views_handlers().
 */

function caldav_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'caldav') . '/handlers',
    ),
    'handlers' => array(
      // Fields
      'caldav_handler_field' => array(
        'parent' => 'views_handler_field',
      ),
      'caldav_handler_field_date' => array(
        'parent' => 'views_handler_field_date',
      ),
    ),
  );
}


/**
 * Implementation of hook_views_plugins().
 */

function caldav_views_plugins() {
  return array(
    'query' => array(
      'caldav_query' => array(
        'title' => t('CalDAV'),
        'help' => t('Queries an external CalDAV source.'),
        'handler' => 'caldav_plugin_query_event',
      ),
    ),
  );
}



//Implementation of hook_date_views_fields().
function caldav_date_views_fields($field) {
    $values = array(
      // The type of date: DATE_UNIX, DATE_ISO, DATE_DATETIME.
      'sql_type' => DATE_DATETIME,
      // Timezone handling options: 'none', 'site', 'date', 'utc'.
      'tz_handling' => 'site',
      // Needed only for dates that use 'date' tz_handling.
      'timezone_field' => '',
      // Needed only for dates that use 'date' tz_handling.
      'offset_field' => '',
      // Array of "table.field" values for related fields that should be
      // loaded automatically in the Views SQL.
      'related_fields' => array(),
      // Granularity of this date field's db data.
      'granularity' => array('year', 'month', 'day', 'hour', 'minute', 'second'),
      );

    switch ($field) {
      case 'caldav.start_date':
      case 'caldav.end_date':
        return $values;
    }
}


function caldav_get_argument_handler($type) {
    switch ($type) {
    case "String":
    case "Memo":
        return 'views_handler_argument';   

    case "Float":
    case "Int":
        return 'views_handler_argument_numeric';

    case "Date":
        return 'views_handler_argument_date';

    case "Boolean":
        return 'views_handler_argument';

    case "StateProvince":
        return 'views_handler_argument';
  
    case "Country":
        return 'views_handler_argument';
        
    case "County":
        return 'views_handler_argument';
    
    default:
        return 'views_handler_argument';
    }
}